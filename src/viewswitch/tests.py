from django.test import TestCase
from django.test import Client
from urllib.parse import urlencode
from django.urls import reverse


class TestViewSwitch(TestCase):

    def test_pass_params(self):
        client = Client()

        response = client.get(reverse('myswitchview'), {'a': [1, 3,5], 'b': 1, 'c': 'ho\\\'la'}, HTTP_ACCEPT='text/csv')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], 'text/csv')

        response = client.post(reverse('myswitchview'), {'a': [1, 3, 5], 'b': 1, 'c': 'ho\\\'la'})
        self.assertEqual(response.status_code, 200)
        response = client.put(reverse('myswitchview'), {'a': [1, 3, 5], 'b': 1, 'c': 'ho\\\'la'})
        self.assertEqual(response.status_code, 200)

        response = client.post(reverse('myswitchview'), {'a': [1, 3, 5], 'b': 1, 'c': 'ho\\\'la'},
                               content_type = "application/json", HTTP_ACCEPT='application/json')

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['Content-Type'], "application/json")
        
        response = client.get(reverse('myswitchview')+"?"+urlencode({'a': [1, 3, 5], 'b': 1, 'c': 'ho\\\'la'}, doseq=True))
        self.assertEqual(response.status_code, 200)
    
        response = client.get(reverse('myswitchview'), HTTP_DATA=urlencode({'a': [1, 3, 5], 'b': 1, 'c': 'ho\\\'la'}, doseq=True))
        self.assertEqual(response.status_code, 200)
        
        clientcookies = Client()
        clientcookies.cookies.load( {'a': [1, 3, 5], 'b': 1, 'c': 'ho\\\'la'})
        response = clientcookies.get(reverse('myswitchview'))
        self.assertEqual(response.status_code, 200)

        clientsession = Client()
        session=clientsession.session
        session.update( {'a': [1, 3, 5], 'b': 1, 'c': 'ho\\\'la'})
        session.save()
        response = clientsession.get(reverse('myswitchview'))
        self.assertEqual(response.status_code, 200)
