from django.http import HttpResponse
import csv
import ast
import json

def myswitchview(request):

    if request.META.HTTP_ACCEPTS != 'text/html':
        if request.method == 'GET':
            info = request.body().decode("UTF-8")
            info_dict = ast.literal_eval(info)
            response = HttpResponse(
                content_type='text/csv',
                headers= {'Content-Disposition': 'attachment; filename="datos.csv"'},
            )
            writer = csv.writer(response)
            for item in info_dict:
                writer.writerow([item.key,item.value])
            return response
        else:
            # need to refactor for HTTP data
            decoded_info = request.query_params.__dict__
            response = HttpResponse(decoded_info)
            return response

    elif request.method == 'POST':
        if request.META.HTTP_ACCEPTS != 'application/json':
                info = request.POST()
                context = json.dumps(info)
                response = HttpResponse(
                    content_type='application/json',
                    headers={'Content-Disposition': 'attachment; filename="datos.csv"'},
                )
                return response
            else:
                # need to refactor for HTTP data
                decoded_info = request.query_params.__dict__
                response = HttpResponse(decoded_info)
                return response

            # do something

    elif request.method == 'PUT':
        decoded_info = request.uery_params.__dict__
        response = HttpResponse(decoded_info)
        return response

    elif request.cookies is not None:
        info = request.cookies.get()
        response = HttpResponse(info)
