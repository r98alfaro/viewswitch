# Prueba práctica.

Se ha creado un proyecto con una app llamada `viewswitch`, la cual posee un conjunto de pruebas que se pueden ejecutar 
de la siguiente manera.

    python manage.py test

Para este ejercicio debe crear un repositorio fork y desarrollar la prueba.
La duracción de la prueba es de una hora.

## Parte #1

En el archivo views.py se creó el esqueleto de la vistas, la cual debe ser usada por usted para recibir los datos enviados en cada 
petición.   La prueba envía los mismos datos en diferentes formas, el sistema debe ser capaz de leerlos en cualquiera de sus formas,
procesar la petición y responder en el formato aceptado en el navegador. 
Así por ejemplo si se solicita un csv, debe crearlo usando los datos recibidos como datos del archivo.

## Parte #2

Cree una vista que reciba siempre los datos por GET, en un formato estándar, use la vista `myswitchview` para procesar los datos de entrada y  redireccionar a la nueva vista creada pasando los datos limpios a la nueva vista.

Puede modificar las pruebas para aceptar el redireccionamiento.

## Parte #3 

Optimice la vista `myswitchview`, para que internamente llame a la vista creada en el ejercicio anterior, sin realizar un direccionamiento a nivel del navegador.

## Parte #4 

Cree un middleware que permita limpiar los datos antes de llegar a la vista, de forma que sea `myswitchview` la encargada de procesar la informacion y responder con el tipo de dato 
solicitado.

Osea cree un middleware capas de leer desde cualquiera de las formas enviada los datos, los ponga en una variable específica ya limpios y en un formato estándar y luego estos datos serán usados por la vista 
para dar una respuesta.